variable "image_id" {
  description = "Image ID"
  type        = string
  default     = "fd81u2vhv3mc49l1ccbb"
}

variable "subnet_id" {
  description = "Subnet ID"
  type        = string
  default     = "e9b1q4ldhksoc83ppj87"
}
