create user reader password 'qwerty1';
create user writer password 'qwerty2';
create user admin password 'qwerty3';
grant select on schema public to reader;
grant update, insert on schema public to writer;
grant all on schema public to admin;
