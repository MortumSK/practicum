-- изменения в таблице product размера колонки name на 20, а колонки picture_url на 100
-- индексы, которые придумали в предыдущем уроке
alter table product alter column name type varchar(20);
alter table product alter column picture_url type varchar(100);
CREATE INDEX idx_product_id ON public.product(id);
CREATE INDEX idx_order_id ON public.orders(id);
CREATE INDEX idx_order_id2 ON public.order_product(order_id);
CREATE INDEX idx_order_product_id ON public.order_product(product_id);
CREATE INDEX idx_c_order_product_ids ON public.order_product(order_id,product_id);
