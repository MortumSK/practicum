#! /bin/bash
#Если свалится одна из команд, рухнет и весь скрипт
set -xe

sudo cp -rf sausage-store-frontend.service /etc/systemd/system/sausage-store-frontend.service
sudo rm -rf /var/www-data/dist/frontend/*
#Переносим артефакт в нужную папку
curl -u ${NEXUS_REPO_USER}:${NEXUS_REPO_PASS} -o sausage-store-front.tar.gz ${NEXUS_REPO_URL}/repository/sausage-store-kim-sergey-frontend/sausage-store/${VERSION}/sausage-store-${VERSION}.tar.gz

sudo tar -zxvf sausage-store-front.tar.gz -C /var/www-data/dist/frontend/ --strip-components=1

#Обновляем конфиг systemd
sudo systemctl daemon-reload
#Перезапускаем сервис сосисочной
sudo systemctl restart sausage-store-frontend
#Проверка статуса сервиса
systemctl status sausage-store-frontend
